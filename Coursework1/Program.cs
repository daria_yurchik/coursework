﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Coursework1
{
    public class Computer
    {
        private int index;
        private String model;
        private double price;
        private String CPU;
        private int RAM;
        public Computer(){iIndex = 0; iModel = ""; iPrice = 0; iCPU = ""; iRAM = 0;}
        public int iIndex {get;set;}
        public String iModel { get; set; }
        public double iPrice { get; set; }
        public String iCPU { get; set; }
        public int iRAM { get; set; }
        public override string ToString()
        {
            string myState;
            myState = string.Format("[iIndex = {0}, iModel = {1}, iPrice = {2}, iCPU = {3}, iRAM = {4}]",
                iIndex, iModel, iPrice, iCPU, iRAM);
            return myState;
        }

    }
    public class ListComputer
    {
        
        List<Computer> computers = new List<Computer>()
        {
            new Computer {iIndex = 0, iModel = "Asus", iPrice = 13800.90, iCPU = "Intel", iRAM = 8 },
            new Computer {iIndex = 1, iModel = "Asus", iPrice = 15734.00, iCPU = "Intel", iRAM = 8 },
            new Computer {iIndex = 2, iModel = "Asus", iPrice = 13455.00, iCPU = "Intel", iRAM = 8 }
        };
        public void display()
        {
            Console.WriteLine("Items in list: {0}", computers.Count);
            foreach (Computer c in computers)
                Console.WriteLine(c);
        }

        public void addElement()
        {
            Console.WriteLine("Add new computer: Index, Model, Price, CPU, RAM.");
            computers.Insert(3, new Computer
            {
                iIndex = Convert.ToInt32(Console.ReadLine()),
                iModel = Console.ReadLine(),
                iPrice = Convert.ToDouble(Console.ReadLine()),
                iCPU = Console.ReadLine(),
                iRAM = Convert.ToInt32(Console.ReadLine())
            });
            foreach (Computer c in computers)
                Console.WriteLine(c);
        }
        public void removeElement()
        {
            Console.WriteLine("Remove element: enter index.");
            computers.RemoveAt(Convert.ToInt32(Console.ReadLine()));
            foreach (Computer c in computers)
                Console.WriteLine(c);
        }
        public void updateElement()
        {
            Console.WriteLine("Enter index element which you would like to change.");
            int i = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter index where you would like to write.");
            int b = Convert.ToInt32(Console.ReadLine());
            computers[i].iIndex = b;
            foreach (Computer c in computers)
                Console.WriteLine(c);
        }
        public void Save()
        {
            Console.WriteLine("Write to file");
            string writePath = @"C:\Users\Дарья\source\repos\Coursework1\data.txt";
            using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
            {
                foreach (Computer c in computers)
                    sw.WriteLine(c);
            }
        }
        public void Load()
        {
            Console.WriteLine("Read from file:");
            string readPath = @"C:\Users\Дарья\source\repos\Coursework1\data2.txt";
            using (StreamReader sr = new StreamReader(readPath, System.Text.Encoding.Default))
            {
                    Console.WriteLine(sr.ReadToEnd());
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ListComputer list = new ListComputer();
            list.display();
            list.addElement();
            list.Load();
            list.removeElement();
            list.updateElement();
            list.Save();
            Console.ReadKey();
        }
    }
}
